package com.solutionsfit.dsl.chatxml

class ChatDslXMLTest extends GroovyTestCase {

    void testChatDslToXML() {
		
        Chat.write("Parcial de Ing Soft II", {
            juan "Hola alumnos, hoy es el parcial"
            alumnos "Hola profesor"
            juan "Paso a repartirlos"
            roman "Piensen los ejemplos vistos en clase, no es nada distinto!"
            lucas "Profe, me da 347 hojas?"
			juan "Ok, pero solo corregimos 2"
			lucas "Si, pero yo escribo mucho en los examenes"
			alumnoDesconocido "*10 minutos antes de entregar* Profe, le entrego mis 4 hojas"
			juan "Ok, cual tiro"
			alumnoDesconocido "*llora*"
            xml
        })
		
    }

}
