package com.solutionsfit.dsl.chatxml

import groovy.xml.MarkupBuilder

/**
 * Processes a simple DSL to create an xml chat
 */
class Chat {

	String chatName
	def sections = []

	/**
	 * This method accepts a closure which is essentially the DSL. Delegate the closure methods to
	 * the DSL class so the calls can be processed
	 */
	def static write(String chatName, closure) {
		Chat chatXML = new Chat()
		chatXML.chatName = chatName
		// any method called in closure will be delegated to the memoDsl class
		closure.delegate = chatXML
		closure()
	}

	/**
	 * When a method is not recognized, assume it is a title for a new section. Create a simple
	 * object that contains the method name and the parameter which is the body.
	 */
	def methodMissing(String methodName, args) {
		def section = new Section(title: methodName, body: args[0])
		sections << section
	}

	/**
	 * 'get' methods get called from the dsl by convention. Due to groovy closure delegation,
	 * we had to place MarkUpBuilder and StringWrite code in a static method as the delegate of the closure
	 * did not have access to the system.out
	 */
	def getXml() {
		doXml(this)
	}

	/**
	 * Use markupBuilder to create a customer xml output
	 */
	private static doXml(Chat memoDsl) {
		def writer = new StringWriter()
		def xml = new MarkupBuilder(writer)
		xml."$memoDsl.chatName"() {
			// cycle through the stored section objects to create an xml tag
			for (s in memoDsl.sections) {
				"$s.title"(s.body)
			}
		}
		println writer
	}

}
